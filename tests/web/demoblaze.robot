*** Settings ***
Resource    ../../resources/Common.resource

Test Setup       Abrir Demoblaze
Test Teardown    Encerra Teste

*** Test Cases ***
TESTE1
    [Documentation]    Criar Conta
    [Tags]             regressao
    Criar Conta
    Login

TESTE2
    [Documentation]    Compra com 3 produtos no carrinho
    [Tags]             regressao
    Criar Conta
    Login
    Adicionar celular ao carrinho
    Adicionar Laptop ao carrinho
    Adicionar Monitor ao carrinho
    Finalizar Compra

TESTE3
    [Documentation]    Adição no carrinho de um celular, um monitor e um computador. 
    ...    Já dentro do carrinho, remova o monitor, atualize a página e confirme a remoção do item
    [Tags]             teste2
    Criar Conta
    Login
    Limpar carrinho
    Adicionar celular ao carrinho
    Adicionar Monitor ao carrinho
    Adicionar Laptop ao carrinho
    Remover Monitor do carrinho


TESTE5
    [Tags]    teste
    LOGIN TESTE
    # Limpar carrinho
    # Adicionar celular ao carrinho
    # Adicionar Laptop ao carrinho
    # Adicionar Laptop ao carrinho
    # Adicionar Monitor ao carrinho

    Remover Monitor do carrinho
    
    